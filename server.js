const { applyMiddleware } = require('graphql-middleware');
const { makeExecutableSchema } = require('graphql-tools');
const {GraphQLServer} = require('graphql-yoga');
const jwt = require("jsonwebtoken")

const resolvers = require("./src/resolvers");
const typeDefs = require("./src/typeDefs");
const middleware = require('./src/middleware');

function auth(req) {
    try {
        return jwt.verify(req.request.get("Authorization").split(" ")[1], "my_secret_key");
    } catch (e) {
        return null;
    }
}

const schema = makeExecutableSchema({ typeDefs, resolvers })
const schemaWithMiddleware = applyMiddleware(schema, middleware)
const server = new GraphQLServer({ 
    schema: schemaWithMiddleware.schema, 
    context: req => ({
        ...req,
        auth: auth(req)
    })    
})

server.start({port: 3000},({port}) => { 
    console.log(`[APP] Server running at http://localhost:${port}`) 
})