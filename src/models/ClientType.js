const AbstractModel = require("./AbstractModel.js");

class ClientType extends AbstractModel {

    table = 'client_type'
    
    entity_name = 'ClientType'

    primary_key = 'id'
    
    atributes = [
        'type',
        'percentage_discount'
    ]

}

module.exports = new ClientType
