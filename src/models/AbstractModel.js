const Connection = require('../core/Database.js')

class AbstractModel {

    arrColumns = []
    limit   = 20
    offset  = 0
    arrHasOne  = []
    arrHasMany = []
    where = ''
    
    listAll()
    {
        const sql = `SELECT * FROM ${this.table}`
        return this.execute(sql)
    }

    listOne(id) 
    {

        if(this.primary_key === undefined) {
            throw "the model should have the 'primary_key' atribute."
        }

        const sql = `SELECT * FROM ${this.table} WHERE ${this.primary_key} = ${id}`
        return this.execute(sql)
    }

    insert(params) 
    {
        let fields = []
        let values = []
  
        if(this.atributes) {        
         fields = fields.concat(this.atributes)
        }
  
        values = '"'+fields.map(field => (params[field]) ? params[field] : null).join('", "')+'"'

        const sql = `INSERT INTO ${this.table} (${fields.join()}) VALUES (${values});`

        return this.execute(sql).then(response => {
            if(this.primary_key) {
                params[this.primary_key] = response.insertId
            }
            return params
        })
    }

    update(params) 
    {
        if(this.primary_key === undefined) {
            throw "the model should have the 'primary_key' atribute."
        }

        const fieldsChange = this.atributes.map( (row) => `${row} = "${params[row]}" `);
        
        const sql = `UPDATE ${this.table} SET ${fieldsChange.join(', ')} WHERE ${this.primary_key} = "${params[this.primary_key]}"; `
        
        return this.execute(sql).then(() => params)
    }

    delete(id) 
    {
        const sql = `DELETE FROM ${this.table} WHERE ${this.primary_key} = ${id}`
        return this.execute(sql).then(() => id)
    }

    execute(sql) 
    {
         return new Promise((resolve, reject) => {
            Connection.query(sql, (erro, resultados, campos) => {
                if (erro) {
                    reject(erro)
                } else {
                    resolve(resultados)
                }
            })
        })
    }

    columns(columns = [])
    {
        this.arrColumns = columns
        return this
    }

    filter(params = [])
    {        
        return this
    }

    paginate(limit = 20, offset = 0)
    {
        this.limit = limit
        this.offset = offset
        return this
    }

    hasOne(entity)
    {
        this.arrHasOne.push(entity)
    }

    hasMany(entity) 
    {
        this.arrHasMany.push(entity)
    }

    getColumns()
    {
        let arrColumn = []
        this.arrColumns.map(column => {
            arrColumn.push(` ${this.table}.${column} ${column} `)
        })
        return arrColumn.join()
    }

    buildQuery()
    {
        let sql = `SELECT `

        // COLUMNS
        let columns = [this.getColumns()]
        this.arrHasOne.forEach(entity => {
            let columnBuilder = ``
            entity.arrColumns.forEach(column => {
                columnBuilder += `'${column}', ${entity.table}.${column},`
            })
            columnBuilder = columnBuilder.substring(0, columnBuilder.length-1)
            columns.push(`JSON_OBJECT(${columnBuilder}) ${entity.entity_name}`) 
        })

        this.arrHasMany.forEach(entity => {
            const indexRelationship = this.relations.hasMany.findIndex(e => e.table == entity.table)
            const relation = this.relations.hasMany[indexRelationship]

            let strColumns = entity.arrColumns.map(column => {
                return ` "${column}", ${relation.table}.${column} ` 
            }).join()

            const whereSub = relation.on.join(" ")
            const subQuery = ` 
            (SELECT 
                JSON_ARRAYAGG(
                    JSON_OBJECT(
                        ${strColumns}
                    )
                )
            FROM  ${relation.table}
            WHERE ${whereSub}
            LIMIT ${entity.limit} 
            OFFSET ${entity.offset}) ${entity.entity_name}
            `

            columns.push(`${subQuery}`)
        })

        sql += columns.join(", ")

        sql += ` FROM ${this.table} `

        // JOINS
        this.arrHasOne.forEach(entity => {
            const indexRelationship = this.relations.hasOne.findIndex(e => e.table == entity.table)
            const relation = this.relations.hasOne[indexRelationship]

            sql += ` ${relation.relationship} JOIN ${relation.table} ON ${relation.on.join(" ")} ` 
        })

        // WHERE
        sql += ` WHERE true ${this.where} `

        // PAGINATION
        sql += `LIMIT ${this.limit} OFFSET ${this.offset};`

        return sql
    }
    
    handleGraphqlResponse(queryResult = [])
    {

        return queryResult.map(row => {

            this.arrHasOne.forEach(e => row[e.entity_name] = JSON.parse(row[e.entity_name]) )
            this.arrHasMany.forEach(e => row[e.entity_name] = JSON.parse(row[e.entity_name]) )

            return row
        })
    }

    async list()
    {
        const sql = this.buildQuery()
        const mainEntityResult = await this.execute(sql)
        return this.handleGraphqlResponse(mainEntityResult)
    }

    close()
    {
        this.arrColumns = []
        this.limit   = 20
        this.offset  = 0
        this.arrHasOne  = []
        this.arrHasMany = []
        this.where = ''        
    }

}

module.exports = AbstractModel
