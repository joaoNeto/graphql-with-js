const AbstractModel = require("./AbstractModel.js");

class Address extends AbstractModel {

    table = 'address'
    
    entity_name = 'Address'

    primary_key = 'id'
    
    atributes = [
        'state',
        'city',
        'street',
        'number_street',
        'address_code',
        'fk_user'
    ]

}

module.exports = new Address
