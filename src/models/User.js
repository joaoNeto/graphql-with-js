const AbstractModel = require("./AbstractModel.js");

class User extends AbstractModel {

    table = 'user'

    primary_key = 'id'

    atributes = [
        'name',
        'email',
        'client_type'
    ]

    relations = {
        hasOne: [
            {
                table: 'client_type',
                on: ["client_type.id", " = ", "user.client_type"],
                relationship: 'INNER'
            }
        ],
        hasMany: [
            {
                table: 'address',
                on: ["address.fk_user", " = ", "user.id"]
            }
        ]
    }

}

module.exports = new User