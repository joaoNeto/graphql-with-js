const mysql = require('mysql')

const CONNECTION = mysql.createConnection({
  host: '172.17.0.1',
  port: 3306,
  user: 'root',
  password: 'root',
  database: 'graphql_database',
  multipleStatements: true
})

module.exports = CONNECTION
