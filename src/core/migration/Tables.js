class Tables {

  init(connection) {
    this.connection = connection
    this.clientTypeTable()
    this.userTable()
    this.addressTable()
    this.adminTable()
    
    console.log('[APP] Tables runned')
  }

  clientTypeTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS client_type (
      id int auto_increment primary key,
      type ENUM('normal', 'premium', 'gold') DEFAULT 'normal',
      percentage_discount float not null
    );    
    `

    this.executeQuery(sql)
  }

  userTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS user (
      id int auto_increment primary key,
      name varchar(100) not null,
      email varchar(100) not null,
      client_type int not null,
      FOREIGN KEY (client_type) REFERENCES client_type(id)
    );
    `

    this.executeQuery(sql)
  }

  addressTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS address (
      id int auto_increment primary key,
      state varchar(100) not null,
      city varchar(100) not null,
      street varchar(100) not null,
      number_street varchar(100) not null,
      address_code varchar(100) not null,
      fk_user int not null,
      FOREIGN KEY (fk_user) REFERENCES user(id)
    );
    `

    this.executeQuery(sql)
  }

  adminTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS admin (
      id int auto_increment primary key,
      name varchar(100) not null,
      email varchar(100) not null,
      password varchar(100) not null
    );
    `
    this.executeQuery(sql)
  }

  executeQuery(sql) {
    this.connection.query(sql, erro => {
      if(erro) {
        console.log(erro)
      }
    })
  }
  
}

module.exports = new Tables
