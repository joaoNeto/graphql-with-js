const FS = require('fs');

class Seeder {
    init(connection) {
        this.connection = connection
        this.client()
        this.user()
        this.address()
        this.admin()
        console.log('[APP] Seeders runned')
    }

    client() {
        const sql = `
        INSERT INTO client_type VALUES 
        (1, 'normal', 0),
        (2, 'premium', 5),
        (3, 'gold', 10);    
        `
        this.executeQuery(sql)
    }

    admin() {
        const sql = `INSERT INTO admin VALUES (1, 'joao neto', 'joao.neto@madeiramadeira.com.br', 'e8d95a51f3af4a3b134bf6bb680a213a');`
        this.executeQuery(sql)
    }

    user() {
        const SQL = FS.readFileSync('./src/core/migration/data/user.sql', 'utf8');
        this.executeQuery(SQL)
    }

    address() {
        const SQL = FS.readFileSync('./src/core/migration/data/address.sql', 'utf8');
        this.executeQuery(SQL)
    }

    executeQuery(sql) {
        this.connection.query(sql, erro => {
            if(erro) {
                console.log(erro)
            }
        })
    }
}

module.exports = new Seeder
