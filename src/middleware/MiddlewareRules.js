
class MiddlewareRules {

    managerParamsRequest(info, args) 
    {
        let response = {}
        response[info.fieldName] = {argument: args, columns: []}

        info.fieldNodes[0].selectionSet.selections.forEach((row) => {
            if(row.selectionSet) {
                const childrens = row.selectionSet.selections.map((childRow) => childRow.name.value)
                response[row.name.value] = {argument: {}, columns: childrens}
            } else {
                response[info.fieldName]['columns'].push(row.name.value)
            }
        })

        return response
    }

    userShouldBeAuth(context)
    {
        if(!context.auth) {
            throw new Error('You must be authenticated to access this entity');
        }
    }

}

module.exports = new MiddlewareRules()
