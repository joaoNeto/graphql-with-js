const MiddlewareRules = require("./MiddlewareRules.js");

module.exports = {
    Query: {
        Users: async (resolve, root, args, context, info) => {

            MiddlewareRules.userShouldBeAuth(context)

            context.client = {}
            context.client.params = MiddlewareRules.managerParamsRequest(info, args)

            return resolve(root, args, context, info)
        }
    }
}

