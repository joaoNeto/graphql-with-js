const ClientTypeModel = require("./../models/ClientType.js");

class ClientType {

    listAll() 
    {
        return ClientTypeModel.listAll()
    }

    async listOne(id) 
    {
        const clientType = await ClientTypeModel.listOne(id)
        return clientType[0]
    }

}

module.exports = new ClientType
