const AddressModel = require("./../models/Address.js");

class Address {

    listAll() 
    {
        return AddressModel.listAll()
    }

    async listOne(id) 
    {
        const addresses = await AddressModel.listOne(id)
        return addresses[0]
    }

    insert(request) 
    {
        return AddressModel.insert(request)
    }

    update(request) 
    {
        return AddressModel.update(request)
    }

    delete(id) 
    {
        return AddressModel.delete(id)
    }

}

module.exports = new Address
