const UserModel = require("./../models/User.js");
const AdminModel = require("./../models/Admin.js");
const AddressModel = require("./../models/Address.js");
const ClientTypeModel = require("./../models/ClientType.js");
const jwt = require("jsonwebtoken")

class User {

    async listAll(args, context) 
    {
        const params = context.client.params

        let response = UserModel.columns(params.Users.columns)
                                .paginate(params.Users.argument.limit, params.Users.argument.offset)

        if(params.ClientType) {
            const columnsClientType = params.ClientType.columns
            const clientType = ClientTypeModel.columns(columnsClientType)
            response.hasOne(clientType)
        }

        if(params.Address) {
            const columnsAddress = params.Address.columns
            const addressModel = AddressModel.columns(columnsAddress).paginate(10, 0)
            response.hasMany(addressModel)
        }
        
        const result = await response.list()
        
        response.close()

        return result
    }

    async listOne(id) 
    {
        const users = await UserModel.list({id})
        return users[0]
    }

    insert(request) 
    {
        return UserModel.insert(request)
    }

    async update(request) 
    {
        UserModel.update(request)
        const user = await this.listOne(request.id)
        return user
    }

    delete(id) 
    {
        return UserModel.delete(id)
    }

    async login(args, context)
    {
        const expiresIn = "2h"

        const admin = await AdminModel.login(args.email, args.password)

        if(admin == undefined) {
            throw new Error('User not found');
        }

        const encrypt = {
            id: admin.id,
            name: admin.name,
            email: admin.email
        }

        const my_token = jwt.sign(encrypt, 'my_secret_key', {expiresIn: expiresIn})

        return {
            userId: admin.id,
            token: my_token,
            tokenExpiration: expiresIn
        }

    }

}

module.exports = new User
