const path = require('path')
const mergeGraphQLSchemas = require('merge-graphql-schemas')

const directory = path.join(__dirname, './')

const {
    fileLoader,
    mergeTypes
} = mergeGraphQLSchemas

const arquivosCarregados = fileLoader(directory)

const schemas = mergeTypes(arquivosCarregados)

module.exports = schemas
