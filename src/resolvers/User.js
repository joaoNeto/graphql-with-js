const User = require("./../services/User");

const resolvers = {
    Query: {
        Users: (root, args, context, info) => User.listAll(args, context),
        User: (root,{id}) => User.listOne(id)
    },
    Mutation: {
        insertUser: (root, request) => User.insert(request),
        updateUser: (root, request) => User.update(request),
        deleteUser: (root, { id }) => User.delete(id)
    }
};

module.exports = resolvers