const ClientType = require("./../services/ClientType");

const resolvers = {
    Query: {
        ClientTypes: () => ClientType.listAll(),
        ClientType: (root, { id }) => ClientType.listOne(id)        
    }
};

module.exports = resolvers