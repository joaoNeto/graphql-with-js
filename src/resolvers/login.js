const User = require("./../services/User");

const resolvers = {
    Query: {
        login: (root, args, context, info) => User.login(args, context)
    },
};

module.exports = resolvers