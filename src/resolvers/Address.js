const Address = require("./../services/Address");

const resolvers = {
  Query: {
    Addresses: () => Address.listAll(),
    Address: (root, {id}) => Address.listOne(id)
  },
  Mutation: {
    insertAddress: (root, request) => Address.insert(request),
    updateAddress: (root, request) => Address.update(request),
    deleteAddress: (root, { id }) => Address.delete(id)
  }
};

module.exports = resolvers