const Connection = require('./src/core/Database.js')
const Tables = require('./src/core/migration/Tables.js')
const Seeders = require('./src/core/migration/Seeders.js')

Connection.connect(erro => {
  if (erro) console.log(erro)
  console.log('[APP] Database connected')
  Tables.init(Connection)
  Seeders.init(Connection)
  console.log('[APP] Finished!!')
})

